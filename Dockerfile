FROM php:7.2-apache
COPY src/ /var/www/html/
RUN rm -f /etc/apache2/sites-enabled/*.conf && chmod -R 777 /var/www/html
COPY sites-enabled/ /etc/apache2/sites-enabled/

